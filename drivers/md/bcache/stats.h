#ifndef _BCACHE_STATS_H_
#define _BCACHE_STATS_H_

struct cache_stat_collector {
	atomic_t cache_hits;
	atomic_t cache_misses;
	atomic_t cache_bypass_hits;
	atomic_t cache_bypass_misses;
	atomic_t cache_readaheads;
	atomic_t cache_miss_collisions;
	atomic_t sectors_bypassed;
	/* added */
	atomic_t seq_sectors_bypassed;
	atomic_t seq_bypassed;
	atomic_t congested_sectors_bypassed;
	atomic_t congested_bypassed;
	atomic_t sectors_completed;
	atomic_t latency;
	atomic_t request_completed;
	atomic_t sectors_hinted;
	atomic_t hinted_completed;
	atomic_t hinted_latency;
	atomic_t cached_sectors;
	atomic_t cached_requests;
	atomic_t incoming_requests;
	atomic_t incoming_sectors;
};

struct cache_stats {
	struct kobject		kobj;

	unsigned long cache_hits;
	unsigned long cache_misses;
	unsigned long cache_bypass_hits;
	unsigned long cache_bypass_misses;
	unsigned long cache_readaheads;
	unsigned long cache_miss_collisions;
	unsigned long sectors_bypassed;
	unsigned long seq_sectors_bypassed;
	unsigned long seq_bypassed;
	unsigned long congested_sectors_bypassed;
	unsigned long congested_bypassed;
	unsigned long sectors_completed;
	unsigned long latency;
	unsigned long request_completed;
	unsigned long sectors_hinted;
	unsigned long hinted_completed;
	unsigned long hinted_latency;
	unsigned long cached_sectors;
	unsigned long cached_requests;
	unsigned long incoming_requests;
	unsigned long incoming_sectors;

	unsigned		rescale;
};

struct cache_accounting {
	struct closure		cl;
	struct timer_list	timer;
	atomic_t		closing;

	struct cache_stat_collector collector;

	struct cache_stats total;
	struct cache_stats five_minute;
	struct cache_stats hour;
	struct cache_stats day;
};

struct cache_set;
struct cached_dev;
struct bcache_device;

void bch_cache_accounting_init(struct cache_accounting *acc,
			       struct closure *parent);

int bch_cache_accounting_add_kobjs(struct cache_accounting *acc,
				   struct kobject *parent);

void bch_cache_accounting_clear(struct cache_accounting *acc);

void bch_cache_accounting_destroy(struct cache_accounting *acc);

void bch_mark_cache_accounting(struct cache_set *, struct bcache_device *,
			       bool, bool);
void bch_mark_cache_readahead(struct cache_set *, struct bcache_device *);
void bch_mark_cache_miss_collision(struct cache_set *, struct bcache_device *);
void bch_mark_sectors_bypassed(struct cache_set *, struct cached_dev *, int);

void bch_mark_sectors_seq_bypassed(struct cache_set *c, struct cached_dev *dc, int sectors);
void bch_mark_sectors_congested_bypassed(struct cache_set *c, struct cached_dev *dc, int sectors);
void bch_mark_sectors_completed(struct cache_set *c, struct cached_dev *dc, int sectors, unsigned long latency);
void bch_mark_sectors_hinted(struct cache_set *c, struct cached_dev *dc, int sectors, unsigned long latency);
void bch_mark_cached_request(struct cache_set *c, struct cached_dev *dc, int sectors);
void bch_mark_requests(struct cache_set *c, struct cached_dev *dc, int sectors);

#endif /* _BCACHE_STATS_H_ */
